/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualpet;

/**
 *
 * @author lehti
 */
public class VirtualPet{
    
    public VirtualPet(){
        //super();
        //app initialization
        
        // Initialize a pet
        //Creature pet;
        
        // Game loop  
    }
   
    private static Creature initCreature(){
        //create a cat
        Creature creature = new Cat();
        
        creature.setName("Rolle");
        creature.setAge(1);
        creature.setSize(4);
        creature.setHappiness(75);
        
        return creature;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //EventQueue.invokeLater(() -> {
        //    VirtualPet ex = new VirtualPet();
        //});
        
        Creature model = initCreature();
        CreatureView view = new CreatureView(model);
        
        GameController controller = new GameController(model, view);
        //controller.updateView();
    }
}//class

