/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualpet;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author lehti
 */
public class CreatureView implements ActionListener{
    private Creature creature;
    private Board imageBoard = new Board();
    
    //constructor that loads creature object into class scope,
    //initializes and draws the ui and loads pictures into a spritesheet
    public CreatureView(Creature creature){
        this.creature = creature;
        
        try {
            imageBoard.loadImage(creature);       
        } catch (IOException ex) {
            
        }
        
        initUI();
    }
    
    // frame to contain all the visuals
    private JFrame frm1 = new JFrame();
    //private Board imageBoard = new Board();
    // buttons
    private JButton feedButton = new JButton("Feed");
    private JButton playButton = new JButton("Play");
    private JButton sleepButton = new JButton("Sleep");
    
    //textboxes
    private JLabel nameLabel = new JLabel("Name");
    private JTextField nameField = new JTextField(30);
    private JLabel ageLabel = new JLabel("Age");
    private JTextField ageField = new JTextField(30);
    private JLabel hungerLabel = new JLabel("Hunger");
    private JTextField hungerField = new JTextField(30);
    private JLabel happinessLabel = new JLabel("Happiness");
    private JTextField happinessField = new JTextField(30);
    private JLabel sleepinessLabel = new JLabel("Sleepiness");
    private JTextField sleepinessField = new JTextField(30);
    
    //panels for layot
    private JPanel textPanel;
    private JPanel imagePanel;
    private JPanel buttonsPanel;
    //graphics component for image drawing
    private Graphics g;
    
    private void initUI() {
        frm1.setTitle("Virtual Pet");
        frm1.setSize(600, 600);
        frm1.setResizable(false);
        frm1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm1.setLocationRelativeTo(null);
        //frames layout config 3 rows, 1 cols
        frm1.setLayout(new GridLayout(3,1));
        frm1.setVisible(true);
        
        //panels for layot
        this.textPanel = new JPanel();
        this.imagePanel = new JPanel();
        this.buttonsPanel = new JPanel();
        textPanel.setLayout(new GridLayout(5,2));
        imagePanel.setLayout(new GridLayout(1,1));
        buttonsPanel.setLayout(new GridLayout(3,1));
        
        //text boxes
        textPanel.add(nameLabel);
        textPanel.add(nameField);
        nameField.setEditable(false);
        textPanel.add(ageLabel);
        textPanel.add(ageField);
        ageField.setEditable(false);
        textPanel.add(hungerLabel);
        textPanel.add(hungerField);
        hungerField.setEditable(false);
        textPanel.add(happinessLabel);
        textPanel.add(happinessField);
        happinessField.setEditable(false);
        textPanel.add(sleepinessLabel);
        textPanel.add(sleepinessField);
        sleepinessField.setEditable(false);
        
        //place for the image to be drawn
        imagePanel.add(imageBoard);
        
        //buttons
        buttonsPanel.add(feedButton);
        buttonsPanel.add(playButton);
        buttonsPanel.add(sleepButton);
        //add buttonsPanel to bottom
        Container cp = frm1.getContentPane();
        cp.setLayout(new BorderLayout());
        cp.add(buttonsPanel, BorderLayout.SOUTH);
        cp.add(imagePanel, BorderLayout.CENTER);
        cp.add(textPanel, BorderLayout.NORTH);
        
        feedButton.addActionListener(this);
        playButton.addActionListener(this);
        sleepButton.addActionListener(this);
        updateUI();
    }
    
    //method that updates all text fields and image
    public void updateUI(){
        nameField.setText(creature.getName());
        ageField.setText(creature.getAgeString());
        hungerField.setText(creature.getHungerString());
        happinessField.setText(creature.getHappinessString());
        sleepinessField.setText(creature.getSleepinessString());
        
        //get graphics component from imagePanel component to draw an image to
        g = imagePanel.getGraphics();
        //call method to draw the loaded pic
        imageBoard.drawCreature(g);
    }
    
    //button actions 
    @Override
    public void actionPerformed(ActionEvent e) {
        Object s = e.getSource();
        if (s == feedButton) {
            creature.decreaseHunger(1); 
        } 
        else if (s == playButton) {
            creature.increaseHappiness();
        } 
        else if (s == sleepButton) {
            creature.decreaseSleepiness(10);
        }
    }
}


//class for image panel
class Board extends JPanel{
    
    private Image cat;
    private Image cat2;
    private Image cat3;
    private BufferedImage sprite;
    //set used for spriteSheet doesnt allow duplicates todo
    private Set<BufferedImage> spriteSheet;
    private int subImageWidth = 200;
    private int subImageHeight =  200;
    
    public void loadImage(Creature creature) throws IOException {
        //load raw sprite sheet
        String imagefile = creature.getImageFile();
        sprite = ImageIO.read(new File("src/resources/" + imagefile));
        //get sub images from the sheet
        //todo
        //get subimage from coords 0x0y with xwidth yheight
        cat = sprite.getSubimage(0,0,subImageWidth,subImageHeight);
        cat2 = sprite.getSubimage(subImageWidth,0,subImageWidth,subImageHeight);
        cat3 = sprite.getSubimage(subImageWidth*2,0,subImageWidth,subImageHeight);
        
        //nullpointer error hmmh
        //spriteSheet.add((BufferedImage) cat);
    }
    
    public void drawCreature(Graphics g){
        g.drawImage(cat, 0, 0, this);
    }
}
