/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualpet;

/**
 *
 * @author lehti
 */
public class GameController {
    private Creature model;
    private CreatureView view;
    
    private final boolean DEBUGMSG = true;
    //game states
    private boolean running = true;
    // tickrate of the game
    final private double GAME_FPS = 1.0;
    // Calculate how many ns each frame should take for our target updates per second.
    final private double TIME_BETWEEN_UPDATES = 1000000000 / GAME_FPS;
    int tick = 0;
    
    public GameController(Creature model, CreatureView view){
        this.model = model;
        this.view = view;
        
        
        gameLoop();
    }
    
    public void updateView(){
        //do visual update
    }
    
     
    //Starts a new thread and runs the game loop in it.
    public void runGameLoop() {
        Thread loop = new Thread() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        loop.start();
    }
    
    // Gameloop itself
    private void gameLoop(){
        // Last time game was updated
        double lastUpdateTime = System.nanoTime();
        while(running){
            double now = System.nanoTime();
            // Update game once TIME_BETWEEN_UPDATES has passed
            while(now - lastUpdateTime > TIME_BETWEEN_UPDATES){
                doGameUpdates();
                lastUpdateTime += TIME_BETWEEN_UPDATES;
            }
        }
    }
    private void doGameUpdates() {
        //System.out.println("game update! " + tick);
        if (DEBUGMSG) {
            model.printInfoDebug();
        }
        model.lifeCycle(tick);
        view.updateUI();
        tick++;
    }
    
}
