/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualpet;

/**
 *
 * @author lehti
 */
public class Cat extends Creature {
    
    //constructor
    public Cat(){
        super();
        super._SPRITESHEET = "cat2.png";
    }

    @Override
    public void printInfo() {
        System.out.println("Miau im a cat, my name is " + getName() + " and i am " + getHappinessString());
        System.out.println("My name age " + getAge() );
        System.out.println("I am a " + getSizeString() + " cat" );
    }
}
