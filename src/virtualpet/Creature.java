/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualpet;

/**
 *
 * @author lehti
 */
public class Creature {
    protected String _SPRITESHEET = "default.png";
    
    //min max attributes, change these depending on creature
    protected int _AGEMULT = 7;
    protected int _MAX_AGE = 100;
    protected int _MIN_AGE = 1;
    protected int _MAX_SIZE = 8;
    protected int _MIN_SIZE = 1;
    
    //food
    protected int _FOODINTAKE = 2;
    protected int _FOODINTERVAL = 5;
    protected int _MAX_HUNGER = 100;
    protected int _MIN_HUNGER = 0;
    protected int _MAX_HUNGERHAPPY = 60; // happiness starts to drop if hunger higher than this
    
    //sleep
    protected int _SLEEPINESSINTERVAL = 2;
    protected int _SLEEPINESSMULT = 1;
    protected int _MAX_SLEEPINESS = 100;
    protected int _MIN_SLEEPINESS = 0;
    
    //happiness
    protected int _MAX_HAPPINESS = 100;
    protected int _PLAYHAPPINESSBOOST = 5;
    protected int _MIN_HAPPINESS = 0;
    protected int _MAX_CYCLESSINCELASTPLAY = 20;
    
    
    private String name;
    private int age;        // 0-100
    private int hunger;     // 100-0
    private int sleepiness; // 0-100
    private int happiness;  // 0-100
    private int size;       // 1small, 2medium, 4large, 8huge
    private int cyclesSinceLastPlay = 0;
    //private enum creatureState{IDLE, SLEEPING, EATING, JUMPING, DEAD};
    //IDLE, SLEEPING, EATING, JUMPING, DEAD
    private int creatureState = 0;

    public Creature() {
        
    }

    /*
    * Creature Actions
    */
    public void increaseHunger() {
        //decrease happiness if hunger gets too high
        if (this.hunger > _MAX_HUNGERHAPPY){
            decreaseHappiness();
        }
        //increase hunger based on creature size
        if (this.hunger >= _MAX_HUNGER)
            this.hunger = _MAX_HUNGER;
        else{
            this.hunger += _FOODINTAKE * this.size;
        }
    }
    public void decreaseHunger(int foodSize) {
        if (this.hunger <= _MIN_HUNGER){
            this.hunger = _MIN_HUNGER;
        }
        else{
            this.hunger -= _FOODINTAKE * foodSize;
        }
    }
    
    public void increaseHappiness(){
        if(this.happiness >= _MAX_HAPPINESS){
            this.happiness = _MAX_HAPPINESS;
        }
        else{
            this.happiness = this.happiness + _PLAYHAPPINESSBOOST;
        }
        // playing needs resources, 
        // increase sleepiness and hunger (according to size)
        increaseSleepiness();
        increaseHunger();
        // reset cyclesSinceLastPlay counter
        cyclesSinceLastPlay = 0;
    }
    public void decreaseHappiness(){
        //prevent happiness going negative
        if (this.happiness <= _MIN_HAPPINESS){
            this.happiness = _MIN_HAPPINESS;
        }
        else{
            // multiply happiness reduction if not played with in a while
            // 1 per day
            if(cyclesSinceLastPlay > _MAX_CYCLESSINCELASTPLAY){
                int penalty = (cyclesSinceLastPlay - _MAX_CYCLESSINCELASTPLAY);
                //System.out.println("happ penalty: " + penalty);
                if (penalty > this.happiness){
                    this.happiness = _MIN_HAPPINESS;
                }
                else{
                    this.happiness = this.happiness - penalty;
                }
            }
            else{
                this.happiness --;
            }
        }
    }
    
    public void increaseSleepiness() {
        if (this.sleepiness >= _MAX_SLEEPINESS)
            this.sleepiness = _MAX_SLEEPINESS;
        else{
            this.sleepiness = this.sleepiness + _SLEEPINESSMULT;
        }
    }
    public void decreaseSleepiness(int timeSlept) {
        if (this.sleepiness <= _MIN_SLEEPINESS){
            this.sleepiness = _MIN_SLEEPINESS;
        }
        else{
            this.sleepiness += 1 * timeSlept;
        }
    }
    
    public void increaseAge(int tick){
        if (tick >= _AGEMULT){
            this.age++;
        }
    }

    /*
    * GET / SET
    */
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        if (age >= _MAX_AGE)
            this.age = _MAX_AGE;
        else if (age <= _MIN_AGE){
            this.age = _MIN_AGE;
        }
        else{
            this.age = age;
        }
    }
    public int getHunger(){
        return hunger;
    }
    public void setHunger(int hunger){
        if (hunger >= _MAX_HUNGER)
            this.hunger = _MAX_HUNGER;
        else if (hunger <= _MIN_HUNGER){
            this.hunger = _MIN_HUNGER;
        }
        else{
            this.hunger = hunger;
        }
    }
    public int getHappiness(){
        return happiness;
    }
    public void setHappiness(int happiness){
        if (happiness >= _MAX_HAPPINESS)
            this.happiness = _MAX_HAPPINESS;
        else if (happiness <= _MIN_HAPPINESS){
            this.happiness = _MIN_HAPPINESS;
        }
        else{
            this.happiness = happiness;
        }
    }
    public int getSleepiness(){
        return sleepiness;
    }
    public void setSleepiness(int sleepiness){
        if (this.sleepiness >= _MAX_SLEEPINESS)
            this.sleepiness = _MAX_SLEEPINESS;
        else if (this.sleepiness <= _MIN_SLEEPINESS){
            this.sleepiness = _MIN_SLEEPINESS;
        }
        else{
            this.sleepiness = sleepiness;
        }
    }
    public int getSize(){
        return this.size;
    }
    public void setSize(int size){
        if (size >= _MAX_SIZE)
            this.size = _MAX_SIZE;
        else if (size <= _MIN_SIZE){
            this.size = _MIN_SIZE;
        }
        else{
            this.size = size;
        }
    }
    
    /*
    * textbox outputs
    */
    public String getAgeString(){
        return String.valueOf(age);
    }
    public String getSizeString(){
        String sizeString;
        switch(this.size){
            case 1:
                sizeString = "small";
                break;
            case 2:
                sizeString = "medium";
                break;
            case 4:
                sizeString = "large";
                break;
            case 8:
                sizeString = "huge";
                break;
            default: 
                sizeString = "Invalid size";
                break;
        }     
        return sizeString;
    }
    public String getHungerString(){
        String hungerString = null;
        if (this.hunger <= 5 * _FOODINTAKE){
                hungerString = "stuffed";
        }
        else if (this.hunger > 5 * _FOODINTAKE && this.hunger <= 20 * _FOODINTAKE){
                hungerString = "just ate";
        }
        else if (this.hunger > 20 * _FOODINTAKE && this.hunger <= 40 * _FOODINTAKE){
                hungerString = "light hunger";
        }
        else if (this.hunger > 40 * _FOODINTAKE && this.hunger <= 75 * _FOODINTAKE){
                hungerString = "hungry";
        }
        else if (this.hunger > 75 * _FOODINTAKE && this.hunger <= 90 * _FOODINTAKE){
                hungerString = "empty stomach";
        }
        else if (this.hunger > 90 * _FOODINTAKE && this.hunger <= 98 * _FOODINTAKE){
                hungerString = "starving";
        }
        else if (this.hunger > 98 * _FOODINTAKE){
                hungerString = "dying of hunger";
        }
        return hungerString;
    }
    public String getHappinessString(){
        String happinessString = null;
        if (this.happiness <= 10){
            happinessString = "miserable";
        }
        else if (this.happiness > 10 && this.happiness <= 25){
            happinessString = "sad";
        }
        else if (this.happiness > 25 && this.happiness <= 45){
            happinessString = "feeling down";
        }
        else if (this.happiness > 45 && this.happiness <= 75){
            happinessString = "ok";
        }
        else if (this.happiness > 75 && this.happiness <= 100){
            happinessString = "happy";
        }
        else if (this.happiness > 100){
            happinessString = "ecstatic!";
        }
        return happinessString;
    }
    public String getSleepinessString(){
        String sleepinessString = null;
        if (this.sleepiness <= 5){
            sleepinessString = "hyperactive, lets go!";
        }
        else if (this.sleepiness > 5 && this.sleepiness <= 25){
            sleepinessString = "fully awake and ready to run";
        }
        else if (this.sleepiness > 25 && this.sleepiness <= 50){
            sleepinessString = "energetic";
        }
        else if (this.sleepiness > 50 && this.sleepiness <= 75){
            sleepinessString = "slow";
        }
        else if (this.sleepiness > 75 && this.sleepiness <= 94){
            sleepinessString = "i want to lie down";
        }
        else if (this.sleepiness > 95 && this.sleepiness <= 99){
            sleepinessString = "sleepy ... can't ... open eyes";
        }
        else if (this.sleepiness > 100){
            sleepinessString = "zzzzzzzzzzz";
        }
        return sleepinessString;
    }    
    
    /*
    * visuals
    */


    void printInfo() {
        System.out.println("My name is " + getName() + " and i am " + getHappinessString());
        System.out.println("My name age " + getAge() );
        System.out.println("I am " + getSizeString() );
    }
    void printInfoDebug() {
        System.out.println("name " + getName());
        System.out.println("age " + getAge() );
        System.out.println("size " + getSizeString()  + " int " + getSize());
        System.out.println("happiness " + getHappinessString() + " int " + getHappiness());
        System.out.println("hunger " + getHungerString() + " int " + getHunger());
        System.out.println("cycles since last play " + cyclesSinceLastPlay);
    }
    public void lifeCycle(int tick){
        //grow hunger every FOODINTERVAL tics
        if (tick % _FOODINTERVAL == 0){
            increaseHunger();
        }
        // grow sleepiness every SLEEPINESSINTERVAL tics
        if (tick % _SLEEPINESSINTERVAL == 0){
            increaseSleepiness();
            decreaseHappiness();
        }
        if (tick % _AGEMULT == 0){
            increaseAge(tick);
        }
        cyclesSinceLastPlay++;
    }

    String getImageFile() {
        return _SPRITESHEET;
    }

}
